public class Dealer{
	private DynamicCardArray hand;
	
	public Dealer(){
		this.hand = new DynamicCardArray();
	}
	
	public void setHand(Card value){
		hand.addCard(value);
	}
	
	public Card getCard(int i){
		return this.hand.get(i);
	}
	
	public int hasPairs(){
		this.hand.selectionSort();
		return this.hand.highestCardMatch();
	}
	
	public int handSize(){
		return this.hand.size();
	}
	
	public int getValueOfCard(int i){
		return this.hand.getCardValue(i);
	}
	
	public int getCountOfMatches(){
		return this.hand.matchCount();
	}
	
	public int getRank(){
		return this.hand.rankValue();
	}
	
	public boolean dealerWon(){
		if(dealerWon()==true){
			System.out.println("Sorry, you loose");
		}
		return false;
	}
	
	public String toString(){
		this.hand.selectionSort();
		return "\u001B[35mDealer's hand: \u001B[37m"+this.hand+" ";
	}
}