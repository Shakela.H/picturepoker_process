public class Card{
	private Suit suits;
	
	public Card(Suit suits){
		this.suits = suits;
	}
	
	public Suit getSuit(){
		return this.suits;
	}
	
	public int getValue(){
		return this.suits.getValueOfCard();
	}
	
	public String toString(){
		return ""+this.suits;
	}
}