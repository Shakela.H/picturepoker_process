public class Player{
	private DynamicCardArray hand;
	private String name;
	
	public Player(){
		this.hand = new DynamicCardArray();
	}
	
	public void setName(String newName){
		this.name = newName;
	}
	
	public String getName(){
		return this.name;
	}
	
	public Card getCard(int i){
		return this.hand.get(i);
	}
	
	public void setHand(Card value){
		hand.addCard(value);
	}
	
	public void takeOut(Card newCard,int i){
		if(i < 0 || i > 5){
			throw new IllegalArgumentException("You can only switch out the cards you have on hand");
		}
		this.hand.set(newCard,i-1);
	}
	
	public int hasPairs(){
		return this.hand.highestCardMatch();
	}
	
	public int handSize(){
		return this.hand.size();
	}
	
	public int getValueOfCard(int i){
		return this.hand.getCardValue(i);
	}
	
	public int getCountOfMatches(){
		return this.hand.matchCount();
	}
	
	public int getRank(){
		return this.hand.rankValue();
	}
	
	public int getPointsWon(){
		return this.hand.pointsWon();
	}
	//maybe remove
	public boolean playerWon(){
		if(playerWon() == true){
			System.out.println("Congrats! you won!");
		}
		return false;
	}
	
	public String toString(){
		this.hand.selectionSort();
		return "\u001B[36mYour hand: \u001B[37m"+this.hand+" ";
	}
}