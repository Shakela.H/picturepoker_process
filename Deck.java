public class Deck{
	private DynamicCardArray deck;
	final int numOfSameCards = 8;
	final int firstPosition = 0;
	
	public Deck(){
		this.deck = new DynamicCardArray();
		for(Suit suits : Suit.values()){
			for(int i = 0; i<numOfSameCards; i++){
				this.deck.addCard(new Card(suits));
			}
		}
		this.deck.shuffle();
	}
	
	public Card removeAndAdd(){
		DynamicCardArray newDeck = new DynamicCardArray();
		newDeck.addCard(this.deck.get(0));
		this.deck.removeCard(0);
		return newDeck.get(0);
	}
	
	public Card getCard(int i){
		return this.deck.get(i);
	}

	public String toString(){
		for (int i = 0; i<this.deck.size(); i++){
			System.out.println("index " +i+"; "+this.deck.get(i));
			System.out.println();
		}
	return  "";
	}
}