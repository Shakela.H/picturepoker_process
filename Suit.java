public enum Suit{
	CLOUD(1){
		public String toString(){
			return "\u001B[94m Cloud \u001B[37m";
		}
	},
	MUSHROOM(2){
		public String toString(){
			return "\u001b[38;2;128;64;0m Mushroom \u001B[37m";
		}
	},
	FLOWER(3){
		public String toString(){
			return "\u001B[38;5;208m Flower \u001B[37m";
		}
	},
	LUIGI(4){
		public String toString(){
			return "\u001B[32m Luigi \u001B[37m";
		}
	},
	MARIO(5){
		public String toString(){
			return "\u001B[31m Mario \u001B[37m";
		}
	},
	STAR(6){
		public String toString(){
			return "\u001B[33m Star \u001B[37m";
		}
	};
	
	private final int value;
	
	private Suit(final int value){
		this.value = value;
	}
	
	public int getValueOfCard(){
		return this.value;
	}
}