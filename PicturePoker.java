import java.util.*;

public class PicturePoker{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to the Picture Poker game! Please enter your name: ");
		String playerName = sc.nextLine();
		boolean newGame = false;
		boolean validBet = false; 
		int tokens = 10;
		int bet = 0;
		System.out.println("Hello "+playerName+"! Would you like to start a new game? y/Y for yes, else, press any key");
		String userAnswer = sc.next();
		while(!newGame){ //Game loop
			if(userAnswer.equals("y") || userAnswer.equals("Y")){ //start of new game
				System.out.println("\u001B[36mWallet: \u001B[37m"+tokens+" tokens left");
				Deck deck = new Deck(); //initiallizing the typ, same fore next 2 lines
				Player player = new Player(); 
				Dealer dealer = new Dealer();
				settingHandValues(deck, player, dealer);
				
				askCardSwap(deck, player, sc);
				
				System.out.println(player);
				validBet = false;
				while(!validBet){
					System.out.println("How many tokens would you like to bet?");
					try{
						int betValue = sc.nextInt();
						if(betValue>tokens || betValue<=0){
							System.out.println("Re-enter a valid token value. The least you can bet is 1 token");
						}else{
							bet = betValue;
							validBet = true;
						}
					}
					catch(InputMismatchException e){
						System.out.println("Not a numerical value");
						sc.next();
					}
				}
				tokens -= bet;
				System.out.println("\u001B[36mWallet: \u001B[37m"+tokens);
				int playerHighestValue = findLargestPairPlayer(player);
				int dealerHighestValue = findLargestPairDealer(dealer);
				System.out.println(dealer);
				tokens = checkForWinner(player, dealer, bet, tokens, playerHighestValue, dealerHighestValue);
				System.out.println("\u001B[36mWallet: \u001B[37m"+tokens);
				newGame = askForNewGame(tokens, sc);
			}else{
				System.out.println("Come back when You're ready!");
				newGame = true;
			}
		}
	}

	//helper method for setting hand values 
	public static void settingHandValues(Deck deck, Player player, Dealer dealer){
		int handSize = 5;
		//setting the player's hand 
		for(int i = 0; i < handSize; i++){
			player.setHand(deck.removeAndAdd());
		}
		System.out.println(player);
		//dealer's hand
		for(int i = 0; i < handSize; i++){
			dealer.setHand(deck.removeAndAdd());
		}
	}
	
	private static void askCardSwap(Deck deck, Player player, Scanner sc){
        System.out.println("Would you like to switch out your card? if so, press y. Else, press any button");
        String userAnswer = sc.next();
        
        if (userAnswer.equals("y") || userAnswer.equals("Y")) {
            System.out.println("Choose which cards you would like to remove, enter 0 if you are done with your selections: ");
            boolean[] valuesList = new boolean[5];
            boolean selection = false;
            int previousValue = 0;
            
            while (!selection) {
                try {
                    int index = sc.nextInt();
                    
                    if (index == 0) {
                        selection = true;
                    } else if (containsValueEntered(index, previousValue, valuesList)) {
                        previousValue = index;
                        valuesList[index - 1] = true;
                        player.takeOut(deck.removeAndAdd(), index);
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Not a numerical value");
                    sc.next();
                }
            }
        }
	}
    
	
	public static boolean containsValueEntered(int index, int previousValue, boolean[] valuesList){
        if(index == previousValue){
			System.out.println("You entered this value previously, put something else");
			return false;
		}else if(index < 1 || index > 5){
			System.out.println("Values can only be between 1 to 5 to switch out");
			return false;
		}else if(valuesList[index-1]){
			System.out.println("You already entered that value...");
			return false;
		}
		return true;
	}
	
	//helper method for checking largest pair
	public static int findLargestPairPlayer(Player player){
		int highestvalue = player.hasPairs();
		return highestvalue;
	}
	
	public static int findLargestPairDealer(Dealer dealer){
		int highestvalue = dealer.hasPairs();
		return highestvalue;
	}
	
	
	public static int checkForWinner(Player player, Dealer dealer, int bet, int tokens, int playerHighestValue, int dealerHighestValue){
		if(player.getRank() > dealer.getRank()){
			System.out.println("Congrats! you won!");
			System.out.println("You won: "+player.getPointsWon()+" points");
			bet = bet*player.getPointsWon();
			tokens+=bet;
			System.out.println("total won: "+bet);
			return tokens;
		}else if(player.getRank() == dealer.getRank()){
			if(playerHighestValue > dealerHighestValue){
				System.out.println("Congrats! you won!");
				System.out.println("You won: "+player.getPointsWon()+" points");
				bet = bet*player.getPointsWon();
				tokens+=bet;
				System.out.println("total won: "+bet);
				return tokens;
			}else if(playerHighestValue == dealerHighestValue){
				System.out.println("it's a draw");
				System.out.println("You get back your tokens");
				tokens+=bet;
				return tokens;
			}else{
				System.out.println("Sorry, you lost.");
				System.out.println("total lost: "+bet);
				return tokens;
			}
		}else{
			System.out.println("Sorry, you lost.");
			System.out.println("total lost: "+bet);
			return tokens;
		}
	}
	
	private static boolean askForNewGame(int tokens, Scanner sc) {
        System.out.println("Would you like to continue playing? Y if yes, else press any button");
        String userAnswer = sc.next();
        if (userAnswer.equals("y") || userAnswer.equals("Y")) {
			if(tokens == 0){
				System.out.println("Sorry, your wallet is empty. You have nothing to bet. Thanks for playing!");
				return true;
			}else{
				sc.nextLine();
				return false;
			}
        } else {
			System.out.println("Thanks for playing!");
            return true;
        }
    }
}