import java.util.Random;

public class DynamicCardArray{
	private Card[] cards;
	private int next;
	private int countOfMatches =1; 
	private int rank;
	private int points;
	
	//setting the cards array to 5
	public DynamicCardArray(){
		this.cards = new Card[5];
	}
	
	//adds a card in position "next"
	public void addCard(Card addCard){
		if (this.next == this.cards.length && this.next <48){
			doubleInSize();
		}
		this.cards[next] = addCard;
		this.next++;
	}
	
	public int size(){
		return this.next;
	}
	
	public int matchCount(){
		return this.countOfMatches;
	}
	
	public void removeCard(int i){
		for (int j = i; j < this.next; j++){
			this.cards[j] = this.cards[j+1];
		}
		this.next--;
	}
	
	public void doubleInSize(){
		Card[] newSize = new Card[this.cards.length*2];
			for (int i = 0; i<this.cards.length; i++){
				newSize[i] = this.cards[i];
			}
			this.cards = newSize;
	}
	
	//comparing int value of the suit, do i need? might remove
	public boolean containCard(Card addedCard){
		final int firstPosition = 0;
		for(int i = 3; i >=firstPosition ; i--){
			if (this.cards[i].getValue() == addedCard.getValue()){
				System.out.println("debug 1 "+this.cards[i].getValue());
				System.out.println("debug 2 "+addedCard.getValue());
				return true;
			}
		}
		return false;
	}
	
	//validations, rankings and highest card value. This code needs improvement.
	public int highestCardMatch(){
		int firstPositionValue = this.cards[0].getValue();
		final int secondPosition = 1;
		int secondPositionValue = this.cards[1].getValue();
		int thirdPositionValue = this.cards[2].getValue();
		int fourthPositionValue = this.cards[3].getValue();
		int lastPositionValue = this.cards[this.next-1].getValue();
		for(int i = this.next-1; i >= secondPosition; i--){
			if (this.cards[i].getValue() == this.cards[i-1].getValue()){
				this.rank++;
				if(lastPositionValue == firstPositionValue){ //five of a kind. Since the hand is sorted from smallest to largest number, if first number matches the last then it's a five of a kind.
					this.rank = 6;
					this.points = 16;
					return lastPositionValue;
				}else if(lastPositionValue == secondPositionValue || //four of a kind 
						fourthPositionValue == firstPositionValue){
					this.rank = 5;
					this.points = 8;
					if(lastPositionValue == secondPositionValue){ //condition to find the largest value
						return lastPositionValue;
					}else if(firstPositionValue == fourthPositionValue){
						return fourthPositionValue;
					}
				}else if(firstPositionValue == secondPositionValue && secondPositionValue == thirdPositionValue || //three of a kind
						secondPositionValue == thirdPositionValue && thirdPositionValue == fourthPositionValue ||
						thirdPositionValue == fourthPositionValue && fourthPositionValue == lastPositionValue ){
					this.rank = 3;
					this.points = 4;
					if(firstPositionValue == secondPositionValue && secondPositionValue != thirdPositionValue || //three plus a pair
					    lastPositionValue == fourthPositionValue && fourthPositionValue != thirdPositionValue){
						this.rank = 4;
						this.points = 6;
						return thirdPositionValue;
					}else{
						if(firstPositionValue == secondPositionValue && secondPositionValue == thirdPositionValue){
							return thirdPositionValue;
						}else if(secondPositionValue == thirdPositionValue && thirdPositionValue == fourthPositionValue){
							return fourthPositionValue;
						}else{
							return lastPositionValue;
						}
					}
				}
			}
			if(i == secondPosition && this.rank == 1){ //one pair
				for(int j = 1; j <=this.next-1; j++){
					if (this.cards[j].getValue() == this.cards[j-1].getValue()){
						this.points = 2;
						return this.cards[j].getValue();
					}
				}
			}
			if( i == secondPosition && this.rank == 2){//two pair
				for(int j = this.next-1; j >= secondPosition; j--){
					if (this.cards[j].getValue() == this.cards[j-1].getValue()){
						this.points = 3;
						return this.cards[j].getValue();
						
					}
				}
			}
		}
		return 0;
	}

	
	public int rankValue(){
		return this.rank;
	}
	
	public int pointsWon(){
		return this.points;
	}
	

	
	//return the card you're looking for
	public Card get(int i){
		if (i >= this.next){
			throw new ArrayIndexOutOfBoundsException("Invalid index");
		}
		return this.cards[i];
	}
	
	//don't forget to add validation
	//change a value at a specific place by overwriting something
	public void set(Card addedCard, int i){
		this.cards[i] = addedCard;
	}
	
	//inserting a card in the hand (or deck)
	public void insert(Card addedCard, int i){
		for(int j = this.next; j >= i +1; j--){
			if(i<0 || i>this.next){
				throw new IllegalArgumentException("Enter a valid position from 1 to 5");
			}
			this.cards[j] = this.cards[j-1];
		}
		this.cards[i] = addedCard;
		this.next++;
	}
	
	public void sort(){
		for(int i = 0; i< this.next-1; i++){
			if(this.cards[i].equals(null)){
				this.cards[i] = this.cards[i+1];
				this.cards[i+1] = null;
			}
		}
	}
	
	public void shuffle(){
		Random r = new Random();
		for(int i = 0; i< this.next; i++){
			int rand = r.nextInt(this.next-1);
			Card temp = cards[i];
			cards[i] = cards[rand];
			cards[rand] = temp;
		}
	}
	//fix this
	private int findMin(Card[] cards, int start) {
		int lowestPlace = start;
		for (int i = start; i < cards.length; i++) {
			if (cards[i].getValue() < cards[lowestPlace].getValue()) {
			lowestPlace = i;
			}
		}
		return lowestPlace;
	}
	//fix this
	public void swap(Card[] cards, int p1, int p2) {
		Card temp = cards[p1];
		cards[p1] = cards[p2];
		cards[p2] = temp;
	}
	
	//fix this
	public void selectionSort() {
		int smallest = -1;
		for (int i = 0; i < this.cards.length - 1; i++) {
			smallest = findMin(this.cards, i);
			swap(this.cards, smallest, i);
		}
	}
	
	public int getCardValue(int i){
		return this.cards[i].getValue();
	}
	
	public String toString(){
		String index = "|";
		
		for (int i = 0; i<this.cards.length; i++){
				index += " "+ (i+1)+": "+this.cards[i] +" |";
		}
		return index;
	}
}